FROM renovate/renovate:39.191-full

ENV SD_VERSION "v1.0.0"
ENV TASKFILE_VERSION "v3.42.0"
ENV HELMFILE_VERSION "v0.171.0"
ENV KUSTOMIZE_VERSION "v5.3.0"
ENV FLUXCD_VERSION "v2.5.1"
ENV YQ_VERSION "v4.45.1"

USER root

RUN echo "install sponge" && \
    apt-get update && apt-get install moreutils -y && \
    apt-get clean

USER ubuntu

RUN echo "install sd ${SD_VERSION}" && \
    TMP_FOLDER=$(mktemp -d) && \
    cd $TMP_FOLDER && \
    curl -qsL https://github.com/chmln/sd/releases/download/${SD_VERSION}/sd-${SD_VERSION}-x86_64-unknown-linux-musl.tar.gz -o sd.tar.gz && \
    tar -xzvf sd.tar.gz --strip=1 && \
    mv sd ~/bin/ && \
    rm -rf $TMP_FOLDER

RUN echo "install Taskfile ${TASKFILE_VERSION}" && \
    curl -qsL https://github.com/go-task/task/releases/download/${TASKFILE_VERSION}/task_linux_amd64.tar.gz |  \
    tar -C ~/bin/ -xzvf - task

RUN echo "install Helmfile ${HELMFILE_VERSION}" && \
    curl -qsL https://github.com/helmfile/helmfile/releases/download/${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION/v/}_linux_amd64.tar.gz | \
    tar -C ~/bin/ -xzvf - helmfile

RUN echo "install Kustomize ${KUSTOMIZE_VERSION}" && \
    curl -qsL https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz | \
    tar -C ~/bin/ -xzvf - kustomize

RUN echo "install FluxCD ${FLUXCD_VERSION}" && \
    curl -qsL https://github.com/fluxcd/flux2/releases/download/${FLUXCD_VERSION}/flux_${FLUXCD_VERSION/v/}_linux_amd64.tar.gz | \
    tar -C ~/bin/ -xzvf - flux

RUN echo "install YQ ${YQ_VERSION}" && \
    curl -qsL https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64.tar.gz | \
    tar -C ~/bin/ -xzvf - ./yq_linux_amd64 && \
    mv ~/bin/yq_linux_amd64 ~/bin/yq

